import tensorflow as tf

# Convert the model
converter = tf.lite.TFLiteConverter.from_saved_model('/Users/williamtrevis/University-Work/4th Year/Modules/Indavidual Project/pythonenv/stay-awake-geodata-network/saved-models') # path to the SavedModel directory
tflite_model = converter.convert()

# Save the model.
with open('/Users/williamtrevis/University-Work/4th Year/Modules/Indavidual Project/pythonenv/stay-awake-geodata-network/saved-lite-models/model.tflite', 'wb') as f:
  f.write(tflite_model)