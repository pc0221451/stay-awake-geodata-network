from audioop import lin2adpcm
from cProfile import label
import matplotlib.pyplot as plt
from csv import reader, writer
import numpy as np


zero = "agressive breaking"
one = "aggressive acceleration"
two = "agressive left turn"
three = "aggressive right turn"
four = "non aggressive event"

keyMap = [zero, one, two, three, four]


featureToPlotArray = [0,1,2,3,4]



for featureToPlot in featureToPlotArray:

    featureDetected = False
    featurePlotted = False
    unTrimmedDataSet = "/Volumes/My Passport/stay-awake-data/geo-data-net/processed_data/attribute_vectors_combined_events_split.csv"
    trimmedDataSet = "/Volumes/My Passport/stay-awake-data/geo-data-net/processed_data/attribute_vectors_peak_combined.csv"
    
    trainingSet = open(unTrimmedDataSet)
    csvData = np.loadtxt(trainingSet, delimiter=",", skiprows=1)
    x_axis_data = []
    y_Y_axis__mean_data = []
    y_X_axis_mean_data = []


    for line in csvData:
        if featurePlotted==False:
            if int(line[8]) == featureToPlot:
                featureDetected=True
                x_axis_data.append(len(y_Y_axis__mean_data))
                y_X_axis_mean_data.append(line[0])
                y_Y_axis__mean_data.append(line[1])

            elif featureDetected:
                featurePlotted=True


    fig, ax = plt.subplots()


    ax.set_axisbelow(True)

    ax.yaxis.grid(color='gray', linestyle='dotted')
    ax.xaxis.grid(color='gray', linestyle='dotted')


    ax.set_ylabel("Mean Reading")
    ax.set_xlabel("Sliding Vectors - 10 Frames")
    plt.title(keyMap[featureToPlot])

    ax.plot(x_axis_data, y_X_axis_mean_data, label="X Movement")
    ax.plot(x_axis_data, y_Y_axis__mean_data, label="Y Movement")
    plt.legend(loc="upper right")

    plt.ylim([-6, 6])
    
    plt.show()