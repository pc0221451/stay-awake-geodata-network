import shutil
import sys

arguments = sys.argv

if(len(arguments) == 1):
    exit(1)

if arguments[1] == '-t':
    shutil.copy('/Volumes/My Passport/stay-awake-data/facial-landmarks-net/dlib-input-data-coordinates/training-data/training.csv', '/Users/williamtrevis/University-Work/4th Year/Modules/Indavidual Project/pythonenv/stay-awake-drowsy-network/data/training')

if arguments[1] == '-v':
    shutil.copy('/Volumes/My Passport/stay-awake-data/facial-landmarks-net/dlib-input-data-coordinates/validation-data/training.csv', '/Users/williamtrevis/University-Work/4th Year/Modules/Indavidual Project/pythonenv/stay-awake-drowsy-network/data/validation')

if arguments[1] == '-u':
    shutil.copy('/Volumes/My Passport/stay-awake-data/facial-landmarks-net/dlib-input-data-coordinates/unseen-data/training.csv', '/Users/williamtrevis/University-Work/4th Year/Modules/Indavidual Project/pythonenv/stay-awake-drowsy-network/data/unseen')