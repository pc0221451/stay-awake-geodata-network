from __future__ import absolute_import
import numpy as np


def loadTrain():

 
  data75times100 = open('../data/training/training.csv')
  diabetesTrainingData = np.loadtxt(data75times100, delimiter=",", skiprows=1)


  drowsy_x = []
  drowsy_y = []
  
  for line in diabetesTrainingData:
    drowsy_y.append(int(line[8]))
    drowsy_x.append(line[0:8])
  
  
  # Randomly shuffle the data.
  drowsy_xy = list(zip(drowsy_x, drowsy_y))
  np.random.shuffle(drowsy_xy)
  diabetes_x, diabetes_y = zip(*drowsy_xy)

  return diabetes_x, _to_one_hot(diabetes_y, 5)


def loadTest():

  data25 = open('../data/unseen/unseen.csv')
  diabetesTestData = np.loadtxt(data25, delimiter=",", skiprows=1)

  db_x = []
  db_y = []
  
  for line in diabetesTestData:
    db_y.append(int(line[8]))
    db_x.append(line[0:8])

  # Randomly shuffle the data.
  db_xy = list(zip(db_x, db_y))
  np.random.shuffle(db_xy)
  diabetes_x, diabetes_y = zip(*db_xy)

  return diabetes_x, _to_one_hot(diabetes_y, 5)


# Returns: A numpy ndarray of shape [n, num_classes] and dtype `float32`.
def _to_one_hot(indices, num_classes):

  one_hot = np.zeros([len(indices), num_classes], dtype=np.float32)
  one_hot[np.arange(len(indices)), indices] = 1
  return one_hot