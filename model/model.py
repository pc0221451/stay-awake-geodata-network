import argparse
import random
import tensorflow as tf
from tensorflow import keras
import datetime
import numpy as np
from time import time
import csv


from loadData import loadTest, loadTrain


# Neural Network Model Class
class DNN():

  # Constructor
  def __init__(self, epochs, learnRate, batchSize, dropout):
    
    self.epochs = epochs
    self.learnRate = learnRate
    self.batchSize = batchSize
    self.dropout = dropout

  # Print results of model instance
  def printResults(self, accuracy_seen, accuracy_unseen, trainTime):

    print('\n--Hyperparameters--\n')
    print('epochs=%d\nlearn rate=%g\nbatch size=%d\ndropout=%g' % (self.epochs, self.learnRate, self.batchSize, self.dropout))
    print("")

    print("--Results--")
    print('\nAccuracy on the unseen data set: %g' % accuracy_seen)

    print('Accuracy on the unseen set: %g' % accuracy_unseen)
    print("\n")

    print('Train Time: %g' % trainTime)
    print("\n")


  # Train the model based on instance hyperparameters given
  def train(self):

    # instantiting keras tensor + telling it the size of the tensor to expect
    drowsy_x = keras.layers.Input((8,))

    # creating the network layers
    dense1 = keras.layers.Dense(
        64, use_bias=True, name='Dense1', activation='sigmoid')(drowsy_x)

    dropout1 = keras.layers.Dropout(
        self.dropout, name='Dropout1')(dense1)
      
    dense2 = keras.layers.Dense(
         36, use_bias=True, name='Dense2', activation='softmax')(dropout1)

    dropout2 = keras.layers.Dropout(
        self.dropout, name='Dropout2')(dense2)

    dense3 = keras.layers.Dense(
         5, use_bias=True, name='Dense3', activation='softmax')(dropout2)
      
    
    model = keras.models.Model(inputs=[drowsy_x], outputs=[dense3])
    opt = keras.optimizers.Adam(learning_rate=self.learnRate)

    model.compile(loss='categorical_crossentropy', optimizer=opt)
    
    print(model.summary())

    
    #load training and test data from csv files and prepare
    X_train, y_train = loadTrain()
    X_test, y_test = loadTest()

    

    # Configure Tensorboard for post training analysis
    log_dir = "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)
    
    model.fit(np.array(X_train), np.array(y_train), batch_size=self.batchSize, epochs=self.epochs, callbacks=[tensorboard_callback])

    
    if(FLAGS.savemodel == True):
      model.save('/Users/williamtrevis/University-Work/4th Year/Modules/Indavidual Project/pythonenv/stay-awake-geodata-network/saved-models/')


    # Run prediction on the training and test set.
    pred_ys = np.argmax(model.predict(np.array(X_train)), axis=1)
    print(pred_ys)
    
    true_ys = np.argmax(y_train, axis=1)
    print(true_ys)
    final_train_accuracy_seen = np.mean((pred_ys == true_ys).astype(np.float32))
    
    pred_ys = np.argmax(model.predict(np.array(X_test)), axis=1)
    true_ys = np.argmax(y_test, axis=1)
    final_train_accuracy_unseen = np.mean((pred_ys == true_ys).astype(np.float32))

    return final_train_accuracy_seen, final_train_accuracy_unseen

  
def main():

  variations = FLAGS.variations

  epochsMinMax = [10,200]
  learnRateMinMax = [0.001, 0.1]
  batchSizeMinMax = [5, 100]
  dropoutMinMax = [0.1, 0.5]


  finalScores = []
  mappedHyperParams = []
  trainingTimes = []
  
  if(FLAGS.hsearch == True):

    for i in range(variations):

      randomEpochNumber = random.randint(epochsMinMax[0], epochsMinMax[1])
      randomLearnRate = round(random.uniform(learnRateMinMax[0], learnRateMinMax[1]), 3)
      randomBatchSize = random.randint(batchSizeMinMax[0], batchSizeMinMax[1])
      randomDropout = round(random.uniform(dropoutMinMax[0], dropoutMinMax[1]), 3)

      model = DNN(randomEpochNumber, randomLearnRate, randomBatchSize, randomDropout)
      start = time()
      accSeen, accUnseen = model.train()
      trainTime = time()-start
      model.printResults(accSeen, accUnseen, trainTime)

      finalScores.append(accUnseen)
      mappedHyperParams.append(model)
      trainingTimes.append(trainTime)

      with open('/Users/williamtrevis/University-Work/4th Year/Modules/Indavidual Project/pythonenv/stay-awake-geodata-network/results/random_results.csv', 'r+', newline='') as file:
          reader = csv.reader(file)
          writer = csv.writer(file)
          writer.writerow([len(list(reader)), model.epochs, model.batchSize, model.dropout, model.learnRate, accSeen, accUnseen, round(trainTime, 2)])

    bestAccuracy = max(finalScores)
    bestIndex = finalScores.index(bestAccuracy)
    bestModel = mappedHyperParams[bestIndex]
    timeTaken = trainingTimes[bestIndex]

    print('\n--Best HyperParameters--\n')
    print('epochs=%d\nlearn rate=%g\nbatch size=%d\ndropout=%g' % (bestModel.epochs, bestModel.learnRate, bestModel.batchSize, bestModel.dropout))
    print("")

    print("--Results--")
    print('\nAccuracy on the unseen data set: %g' % bestAccuracy)
    print("")

    print('Train Time: %g' % timeTaken)
    print("\n")

  else:

      model = DNN(FLAGS.epochs, FLAGS.learnRate, FLAGS.batchSize, FLAGS.dropout)
      start = time()
      accSeen, accUnseen = model.train()
      trainTime = time()-start
      model.printResults(accSeen, accUnseen, trainTime)

      with open('/Users/williamtrevis/University-Work/4th Year/Modules/Indavidual Project/pythonenv/stay-awake-geodata-network/results/intuition_results.csv', 'r+', newline='') as file:
          reader = csv.reader(file)
          writer = csv.writer(file)
          writer.writerow([len(list(reader)), model.epochs, model.batchSize, model.dropout, model.learnRate, accSeen, accUnseen, round(trainTime, 2)])


if __name__ == '__main__':
  
  parser = argparse.ArgumentParser('Drowsy model training and serialization')
  
  parser.add_argument(
      '--epochs',
      type=int,
      default=10,
      help='Number of epochs to train the Keras model for.')

  parser.add_argument(
      '--learnRate',
      type=float,
      default=0.001)

  parser.add_argument(
      '--batchSize',
      type=int,
      default=8)

  parser.add_argument(
      '--variations',
      type=int,
      default=5)

  parser.add_argument(
      '--hsearch',
      type=bool,
      default=False)

  parser.add_argument(
      '--dropout',
      type=float,
      default=0.2)

  parser.add_argument(
      '--savemodel',
      type=bool,
      default=False)

  FLAGS, _ = parser.parse_known_args()
  main()