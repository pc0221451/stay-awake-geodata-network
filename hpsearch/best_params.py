import numpy as np

randomSearchData = open('/Users/williamtrevis/University-Work/4th Year/Modules/Indavidual Project/pythonenv/stay-awake-geodata-network/results/random_results.csv')
randomSearchCSV = np.loadtxt(randomSearchData, delimiter=",")

bestHP = 0

for counter, row in enumerate(randomSearchCSV):

    if(counter == 0):
        bestHP = row
        continue

    if(float(row[6] > float(bestHP[6]))):
        bestHP = row


print('\n--Best HyperParameters--\n')
print('epochs=%d\nlearn rate=%g\nbatch size=%d\ndropout=%g' % (bestHP[1], bestHP[2], bestHP[3], bestHP[4]))
print("")

print("--Results--")
print('\nAccuracy on the unseen data set: %g' % bestHP[6])
print("")

print('Train Time: %g' % bestHP[7])
print("\n")