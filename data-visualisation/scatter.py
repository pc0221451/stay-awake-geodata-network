import numpy as np
import matplotlib.pyplot as plt

trainingDataSet = "/Users/williamtrevis/University-Work/4th Year/Modules/Indavidual Project/pythonenv/stay-awake-geodata-network/data/training/training.csv"
unseenDataSet = "/Users/williamtrevis/University-Work/4th Year/Modules/Indavidual Project/pythonenv/stay-awake-geodata-network/data/unseen/unseen.csv"

trainingSet = open(trainingDataSet)
data = np.loadtxt(trainingSet, delimiter=",", skiprows=1)


fig, ax = plt.subplots()

ax.set_xlabel("Tendency Value (X)")
ax.set_ylabel("Tendency Value (Y)")

x = data[:, [6, 7]]
y = data[:, -1].astype(int)

dotSize=10

plt.scatter(x[:,0][y==0], x[:,1][y==0], s=dotSize, c='r', label="Aggressive Breaking")
plt.scatter(x[:,0][y==1], x[:,1][y==1], s=dotSize, c='b', label="Aggressive Acceleration")
plt.scatter(x[:,0][y==2], x[:,1][y==2], s=dotSize, c='g', label="Aggressive Left Turn")
plt.scatter(x[:,0][y==3], x[:,1][y==3], s=dotSize, c='purple', label="Aggressive Right Turn")
plt.scatter(x[:,0][y==4], x[:,1][y==4], s=dotSize, c='orange', label="Non-Agressive Event")


plt.legend(loc="upper left")
plt.ylim([-200, 200])
plt.xlim([-200, 200])
plt.show()